﻿using System.Collections.Generic;

namespace TestTask.DM.Console.Models
{
    public class CompoundWord
    {
        public string Name { get; set; }
        public List<string> Substrings { get; set; }

        public override string ToString()
        {
            return $"{string.Join("+", Substrings.ToArray())}={Name}";
        }
    }
}