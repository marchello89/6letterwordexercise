﻿using System;
using System.Collections.Generic;
using System.Linq;
using TestTask.DM.Console.Models;
using TestTask.DM.Console.Services.Interfaces;

namespace TestTask.DM.Console.Services
{
    public class AlgorithmService : IAlgorithmService
    {
        public List<CompoundWord> GetMatchedWords(List<string> substringList, List<string> validWords,
            int wordLength)
        {
            if (substringList == null)
            {
                throw new ArgumentException(nameof(substringList));
            }

            if (validWords == null)
            {
                throw new ArgumentException(nameof(validWords));
            }

            if (wordLength < 2)
            {
                throw new ArgumentException(nameof(wordLength));
            }

            if (validWords.Any(x => x.Length != wordLength))
            {
                throw new ArgumentException(nameof(validWords));
            }

            var result = new List<CompoundWord>();

            if (!validWords.Any() || !substringList.Any())
            {
                return result;
            }

            var prefixesHashSet = GeneratePrefixesHashSet(validWords, wordLength);

            var usedSubstrings = new HashSet<string>();
            for (var i = 0; i < substringList.Count; i++)
            {
                if (usedSubstrings.Contains(substringList[i]) || substringList[i].Length >= wordLength)
                {
                    continue;
                }

                usedSubstrings.Add(substringList[i]);

                var words = GetMatchedWordCombinations(substringList[i], new List<int>(wordLength) {i}, prefixesHashSet,
                    wordLength, substringList);
                result.AddRange(words);
            }

            return result;
        }

        private HashSet<string> GeneratePrefixesHashSet(List<string> validWords, int wordLength)
        {
            var hashSet = new HashSet<string>();
            foreach (var word in validWords)
            {
                for (var i = 1; i <= wordLength; i++)
                {
                    hashSet.Add(word.Substring(0, i));
                }
            }

            return hashSet;
        }

        private List<CompoundWord> GetMatchedWordCombinations(string currentSubstring, ICollection<int> usedIndexes,
            HashSet<string> prefixesHashSet, int wordLength,
            IReadOnlyList<string> substringList)
        {
            var result = new List<CompoundWord>();

            if (!prefixesHashSet.Contains(currentSubstring))
            {
                return result;
            }

            var usedSubstrings = new HashSet<string>();

            for (var i = 0; i < substringList.Count; i++)
            {
                if (usedIndexes.Contains(i))
                {
                    continue;
                }

                if (usedSubstrings.Contains(substringList[i]))
                {
                    continue;
                }

                usedSubstrings.Add(substringList[i]);

                var nextSubstring = currentSubstring + substringList[i];

                if (nextSubstring.Length > wordLength)
                {
                    continue;
                }

                if (nextSubstring.Length < wordLength)
                {
                    var newUsedIndexes = new List<int>(usedIndexes) {i};

                    var words = GetMatchedWordCombinations(nextSubstring, newUsedIndexes, prefixesHashSet, wordLength, substringList);
                    result.AddRange(words);

                    continue;
                }

                if (prefixesHashSet.Contains(nextSubstring))
                {
                    var substrings = new List<string>();
                    substrings.AddRange(usedIndexes.Select(x => substringList[x]));
                    substrings.Add(substringList[i]);

                    var compoundWord = new CompoundWord {Name = nextSubstring, Substrings = substrings};
                    result.Add(compoundWord);
                }
            }

            return result;
        }
    }
}