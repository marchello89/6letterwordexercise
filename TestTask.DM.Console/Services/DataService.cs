﻿using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using TestTask.DM.Console.Services.Interfaces;

namespace TestTask.DM.Console.Services
{
    public class DataService : IDataService
    {
        private readonly ILogger _logger;

        public DataService(ILogger<DataService> logger)
        {
            _logger = logger;
        }

        public async Task<string[]> GetWordsFromFileAsync(string path)
        {
            _logger.LogInformation($"Start reading input file: {path}");

            var result = await File.ReadAllLinesAsync(path, Encoding.UTF8);

            _logger.LogInformation($"{result.Length} - line(s) have been read");

            return result;
        }
    }
}