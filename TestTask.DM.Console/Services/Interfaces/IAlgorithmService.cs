﻿using System.Collections.Generic;
using TestTask.DM.Console.Models;

namespace TestTask.DM.Console.Services.Interfaces
{
    public interface IAlgorithmService
    {
        List<CompoundWord> GetMatchedWords(List<string> substrings, List<string> validWords, int wordLength);
    }
}