﻿using System.Threading.Tasks;

namespace TestTask.DM.Console.Services.Interfaces
{
    public interface IDataService
    {
        Task<string[]> GetWordsFromFileAsync(string path);
    }
}