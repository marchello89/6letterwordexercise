﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TestTask.DM.Console.Services.Interfaces;

namespace TestTask.DM.Console
{
    public class App
    {
        private readonly AppSettings _appSettings;
        private readonly IDataService _dataService;
        private readonly ILogger _logger;
        private readonly IAlgorithmService _algorithmService;

        public App(IOptions<AppSettings> settings, ILogger<App> logger, IDataService dataService, IAlgorithmService algorithmService)
        {
            _logger = logger;
            _dataService = dataService;
            _algorithmService = algorithmService;
            _appSettings = settings.Value;
        }

        public async Task Run()
        {
            try
            {
                var stopwatch = Stopwatch.StartNew();

                //read input file
                var stringCollection = await _dataService.GetWordsFromFileAsync(_appSettings.InputFilePath);

                //prepare data for algorithm
                var substringList = stringCollection.Where(x => x.Length < _appSettings.WordLength).OrderBy(x=>x).ToList();
                var validWordList = stringCollection.Where(x => x.Length == _appSettings.WordLength).ToList();

                var result =_algorithmService.GetMatchedWords(substringList, validWordList,_appSettings.WordLength);
                
                stopwatch.Stop();
                _logger.LogInformation($"Found {result.Count} unique combination(s)");
                                _logger.LogInformation("Operation finished. Taken - " + stopwatch.Elapsed.ToString("mm\\:ss\\.ff"));

                //Print result
                result.ForEach(x =>_logger.LogInformation(x.ToString()));
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
            }
        }
    }
}