﻿namespace TestTask.DM.Console
{
    public class AppSettings
    {
        public string InputFilePath { get; set; }
        public int WordLength { get; set; }

    }
}
