﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Events;
using TestTask.DM.Console.Services;
using TestTask.DM.Console.Services.Interfaces;

namespace TestTask.DM.Console
{
    public class Program
    {
        private static IConfigurationRoot _configuration;

        public static int Main(string[] args)
        {
          Log.Logger = new LoggerConfiguration()
                .WriteTo.Console(LogEventLevel.Information)
                .MinimumLevel.Debug()
                .CreateLogger();

            try
            {
                MainAsync().Wait();

                return 0;
            }
            catch
            {
                return 1;
            }
        }

        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
          _configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
                .AddJsonFile("appsettings.json", false)
                .Build();

            serviceCollection.AddLogging(configure => configure.AddSerilog());

            serviceCollection.AddTransient<App>();
            serviceCollection.AddTransient<IDataService, DataService>();
            serviceCollection.AddTransient<IAlgorithmService, AlgorithmService>();

            serviceCollection.Configure<AppSettings>(options => _configuration.Bind(options));
        }

        private static async Task MainAsync()
        {
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();

            try
            {
                await serviceProvider.GetService<App>().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Error running service");
                throw;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }
    }
}