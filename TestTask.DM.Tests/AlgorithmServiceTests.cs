﻿using System;
using System.Collections.Generic;
using System.Linq;
using TestTask.DM.Console.Services;
using Xunit;

namespace TestTask.DM.Tests
{
    public class AlgorithmServiceTests
    {
        private const int ValidWordLength = 5;
        private const int InvalidWordLength = -1;
        private readonly List<string> _validWords = new List<string> {"denis", "aaaas"};
        private readonly List<string> _validSubstrings = new List<string>
            {"n", "d", "is", "e", "den", "a", "a", "a", "s", "a", "deni", "a", "a", "i", "a", "a"};

        [Fact]
        public void Set_CorrectInput_ReturnsResult()
        {
            //arrange
            var service = new AlgorithmService();

            //act
            var result = service.GetMatchedWords(_validSubstrings, _validWords, ValidWordLength);

            //assert
            Assert.Equal(6, result.Count);
            Assert.Equal(5, result.Count(x => x.Name == _validWords[0]));
            Assert.Equal(1, result.Count(x => x.Name == _validWords[1]));
        }

        [Fact]
        public void Set_EmptySubstringList_ReturnsEmptyResult()
        {
            //arrange
            var service = new AlgorithmService();

            //act
            var result = service.GetMatchedWords(new List<string>(), _validWords, ValidWordLength);

            //assert
            Assert.Empty(result);
        }

        [Fact]
        public void Set_EmptyValidWords_ReturnsEmptyResult()
        {
            //arrange
            var service = new AlgorithmService();

            //act
            var result = service.GetMatchedWords(_validSubstrings, new List<string>(), ValidWordLength);

            //assert
            Assert.Empty(result);
        }

        [Fact]
        public void Set_IncorrectInput_ThrowsArgumentNullException()
        {
            //arrange
            var service = new AlgorithmService();

            //act
            void Act1()
            {
                service.GetMatchedWords(_validSubstrings, null, ValidWordLength);
            }

            void Act2()
            {
                service.GetMatchedWords(null, _validWords, ValidWordLength);
            }

            void Act3()
            {
                service.GetMatchedWords(_validSubstrings, _validWords, InvalidWordLength);
            }

            //assert
            Assert.Throws<ArgumentException>(Act1);
            Assert.Throws<ArgumentException>(Act2);
            Assert.Throws<ArgumentException>(Act3);
        }

        [Fact]
        public void Set_Negative_ReturnsEmptyResult()
        {
            //arrange
            var service = new AlgorithmService();

            //act
            var result = service.GetMatchedWords(new List<string>(), _validWords, ValidWordLength);

            //assert
            Assert.Empty(result);
        }
    }
}